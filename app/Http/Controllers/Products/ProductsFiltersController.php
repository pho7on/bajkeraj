<?php

namespace App\Http\Controllers\Products;

use App\Http\Controllers\Controller;
use App\Models\Products\ProductsFilters;
use App\Models\Products\ProductsFiltersCategories;
use Illuminate\Http\Request;

class ProductsFiltersController extends Controller
{
    public static function addProductFilterCategory(Request $request)
    {
        $rules = [
            "name" => "required|string",
        ];
        $validation = ValidateHttpRequest($rules, $request);
        if ($validation) {
            return response()->json(array_merge($validation));
        }
        $productCategory = ProductsFiltersCategories::create($request->all());
        if (!$productCategory) {
            return response()->json(["success" => false, "message" => "Something went wrong"]);
        }
        return response()->json(["success" => true, "categories" => self::getAllCategories()]);
    }

    private static function getAllCategories()
    {
        $categories = ProductsFiltersCategories::all();
        foreach ($categories as $category) {
            $category->filters = ProductsFilters::where("category_id", "=", $category->id)->get();
        }

        return $categories;
    }


    public static function getAllProductFilterCategories()
    {
        $categories = self::getAllCategories();
        return response()->json(["success" => true, "categories" => $categories]);
    }

    public static function editProductFilterCategory(Request $request)
    {
        $rules = [
            "id" => "required",
            "name" => "required"
        ];

        $validation = ValidateHttpRequest($rules, $request);
        if ($validation) {
            return response()->json(array_merge($validation));
        }

        $productCategory = ProductsFiltersCategories::find($request->get("id"));

        if (!$productCategory) {
            return response()->json(["success" => false, "message" => "Product category not found"]);
        }
        $productCategory->name = $request->get("name");
        $productCategory->save();
        return response()->json(["success" => true, "categories" => self::getAllCategories()]);
    }

    public static function deleteProductFilterCategory(Request $request)
    {
        $rules = [
            "id" => "required",
        ];
        $validation = ValidateHttpRequest($rules, $request);
        if ($validation) {
            return response()->json(array_merge($validation));
        }
        $productCategory = ProductsFiltersCategories::find($request->get("id"));
        try {
            $productCategory->delete();
        } catch (\Exception $e) {
            return response()->json(["success" => false, "message" => $e]);
        }
        return response()->json(["success" => true, "categories" => self::getAllCategories()]);
    }

    public static function createProductFilter(Request $request)
    {
        $rules = [
            "category_id" => "required",
            "name" => "required"
        ];
        $validation = ValidateHttpRequest($rules, $request);
        if ($validation) {
            return response()->json(array_merge($validation));
        }
        $productFilter = ProductsFilters::create($request->all());
        if (!$productFilter) {
            return response()->json(["success" => false, "message" => "Something went wrong"]);
        }

        return response()->json(["success" => true, "categories" => self::getAllCategories()]);
    }

    public static function getAllProductFilters()
    {
        $productFilters = self::getAllCategories();
        return response()->json(["success" => true, "categories" => $productFilters]);
    }

    public static function updateProductFilter(Request $request)
    {
        $rules = [
            "id" => "required",
        ];
        $validation = ValidateHttpRequest($rules, $request);
        if ($validation) {
            return response()->json(array_merge($validation));
        }

        $productFilter = ProductsFilters::find($request->get("id"));
        if (!$productFilter) {
            return response()->json(["success" => false, "message" => "Product filter not found"]);
        }
        $productFilter->update($request->except("id"));
        $productFilter->save();

        if (!$productFilter) {
            return response()->json(["success" => false, "message" => "Something went wrong"]);
        }

        return response()->json(["success" => true, "message" => self::getAllCategories()]);

    }

    public static function deleteProductFilter(Request $request)
    {
        $rules = [
            "id" => "required",
        ];
        $validation = ValidateHttpRequest($rules, $request);
        if ($validation) {
            return response()->json(array_merge($validation));
        }

        $productFilter = ProductsFilters::find($request->get("id"));
        if (!$productFilter) {
            return response()->json(["success" => false, "message" => "Product filter not found"]);
        }
        try {
            $productFilter->delete();
            return response()->json(["success" => true, "products" => self::getAllCategories()]);
        } catch (\Exception $e) {
            return response()->json(["success" => false, "message" => $e]);
        }
    }
}
