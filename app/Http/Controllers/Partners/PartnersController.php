<?php

namespace App\Http\Controllers\Partners;

use App\Http\Controllers\Controller;
use App\Models\Partners\PartnersModel;
use App\Traits\HandleImageOptimization;
use App\Traits\HandleImageUpload;
use Illuminate\Http\Request;

class PartnersController extends Controller
{
    use HandleImageUpload, HandleImageOptimization;

    public static function addPartner(Request $request)
    {
        $rules = [
            "image" => "required",
            "type" => "required",
            "order" => "required"
        ];
        $validation = ValidateHttpRequest($rules, $request);
        if ($validation) {
            return response()->json(array_merge($validation));
        }

        $image = $request->file('image');

        $uploaded = self::imageUploadHandler([$image], "partners");

        $optimization = self::handleImageOptimization(1920, true, $uploaded['images'], public_path($uploaded['path']));

        $partner = PartnersModel::create(
            [
                "image" => $uploaded['path'] . $uploaded['images'][0],
                "type" => $request->get("type"),
                "order" => $request->get("order"),
                "link" => $request->get("link")
            ]
        );

        if (!$partner) {
            return response()->json(["success" => false, "message" => "Something went wrong"]);
        }
        return response()->json(["success" => true, "partner" => $partner]);
    }

    public static function getAllPartners(Request $request)
    {
        $type = $request->get("type") || 0;
        return response()->json(["success" => true, "partners" => PartnersModel::where("type", "=", $type)->orderBy("order")->get()]);
    }

    public static function deletePartner(Request $request)
    {
        $partner = PartnersModel::find($request->get("id"));

        try {
            $partner->delete();
            return response()->json(["success" => true, "message" => "Partner deleted", "partners" => PartnersModel::where("type", "=", 0)->orderBy("order")->get()]);
        } catch (\Exception $e) {
            return response()->json(["success" => false, "message" => $e,]);
        }
    }

    public static function editPartner(Request $request)
    {
        $rules = [
            "id" => "required|int"
        ];

        $validation = ValidateHttpRequest($rules, $request);
        if ($validation) {
            return response()->json(array_merge($validation));
        }
        $data = [];
        if ($request->hasFile("logo")) {
            $images = [$request->file('logo')];
            $uploaded = self::imageUploadHandler($images, "partners");
            $optimization = self::handleImageOptimization(1920, true, $uploaded['images'], public_path($uploaded['path']));
            $data = array_merge($data, ["image" => $uploaded['path'] . $uploaded['images'][0]]);
        }

        $data = array_merge($data, ["link" => $request->get('link')]);

        $partner = PartnersModel::find($request->get("id"))->update($data);

        if (!$partner) {
            return response()->json(["success" => false, "message" => "Something went wrong"]);
        }
        return response()->json(["success" => true, "message" => "Competition edited successfully", "competitions" => PartnersModel::where("type", "=", 0)->orderBy("order")->get()]);
    }

    public static function sortPartners(Request $request)
    {
        $partners = $request->get("partners");
        $counter = 0;
        $type = $request->get("type") || 0;
        foreach ($partners as $partner) {
            $m = PartnersModel::where('id', '=', $partner['id'])->where("type", "=", $type)->update(['order' => $counter]);
            $counter++;
        }
        return response()->json(["success" => true, "menus" => $partners]);
    }
}
