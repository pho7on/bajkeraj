<?php
Route::group(
    ['prefix' => 'products/'],
    function () {
        Route::group(
            ['middleware' => 'auth:api'],
            function () {
                Route::post('/addProduct', 'Products\ProductsController@addProduct');
                Route::post('/deleteProduct', 'Products\ProductsController@deleteProduct');
                Route::post('/updateProduct', 'Products\ProductsController@updateProduct');
                Route::post('/addImages', 'Products\ProductsController@addImages');
                Route::post('/deleteImage', 'Products\ProductsController@deleteImage');
                Route::post('/sortProducts', 'Products\ProductsController@sortProducts');
                Route::post('/toggleSpecialOffer', 'Products\ProductsController@toggleSpecialOffer');
                Route::post('/togglePublished', 'Products\ProductsController@togglePublished');
                Route::post('/updateProductCode', 'Products\ProductsController@updateProductCode');
                //FILTERS
                Route::post('/addProductFilterCategory', 'Products\ProductsFiltersController@addProductFilterCategory');
                Route::post('/editProductFilterCategory', 'Products\ProductsFiltersController@editProductFilterCategory');
                Route::post('/deleteProductFilterCategory', 'Products\ProductsFiltersController@deleteProductFilterCategory');

                Route::post('/createProductFilter', 'Products\ProductsFiltersController@createProductFilter');
                Route::post('getAllProductFilters', 'Products\ProductsFiltersController@getAllProductFilters');
                Route::post('updateProductFilter', 'Products\ProductsFiltersController@updateProductFilter');
                Route::post('deleteProductFilter', 'Products\ProductsFiltersController@deleteProductFilter');
            });
        Route::post('/addProductCategory', 'Products\ProductsController@addProductCategory');
        Route::post('/getAllProductsForSorting', 'Products\ProductsController@getAllProductsForSorting');
        Route::post('/getAllProducts', 'Products\ProductsController@getAllProducts');
        Route::post('/getProduct', 'Products\ProductsController@getProduct');
        Route::post('/getAllProductsRabat', 'Products\ProductsController@getAllProductsRabat');
        //FILTERS
        Route::post('/getAllProductFilterCategories', 'Products\ProductsFiltersController@getAllProductFilterCategories');

    });
