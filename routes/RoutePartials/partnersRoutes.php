<?php
Route::group(
    ['prefix' => 'partners/'],
    function () {
        Route::group(
            ['middleware' => 'auth:api'],
            function () {
                Route::post('/addPartner', 'Partners\PartnersController@addPartner');
                Route::post('/deletePartner', 'Partners\PartnersController@deletePartner');
                Route::post('/editPartner', 'Partners\PartnersController@editPartner');
                Route::post('/sortPartners', 'Partners\PartnersController@sortPartners');
            });
        Route::post('/getAllPartners', 'Partners\PartnersController@getAllPartners');
    });


