<?php

Route::get('/', function () {
    return view('welcome');
});
Route::get('/confirm/{token}', 'Customers\CustomersController@confirmUser');
Route::get('/getNewProducts', 'Products\ProductsController@getNewProducts');
Route::get('/pdfStyle', function () {
    return view("orderPDF");
});
Route::get('/test', 'Orders\OrdersController@renderPDF');
Route::get('/parseXML', 'Products\ProductsController@parseXML');

Route::get('{any}', function () {
    return view('welcome');
});

Route::get('/{any}', function () {
    return view('welcome');
})->where('any', '.*');


